import path from 'path';
import fs from 'fs';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const env = process.env;
const PROJECT_ROOT = path.normalize(`${__dirname}/../..`);
const SRC = `${PROJECT_ROOT}/src`;

module.exports = {

  entry: [
    'babel-polyfill',
    `${SRC}/resources/assets/js/index.js`,
  ],

  output: {
    path: `${SRC}/public/assets`,
    publicPath: '/assets',
    filename: 'bundle.js',
  },

  watchOptions: {
    poll: true
  },
  watch: true,

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract(['css-loader', 'sass-loader']),
      },
      {
        test: /\.(jpg|jpeg|gif|png)$/,
        use:'file-loader?name=images/[name].[ext]',
        exclude: /node_modules/,
      },
      {
        test: /\.(woff|woff2|eot|otf|ttf|svg)$/,
        use:'file-loader?name=fonts/[name].[ext]',
        exclude: /node_modules/,
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
    ],
  },

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin({ filename: 'styles.css', allChunks: true }),
    new webpack.DefinePlugin({
      'process.env': {
        'APP_DOMAIN': JSON.stringify(env.APP_DOMAIN),
        'ENVIRONMENT': JSON.stringify(env.ENVIRONMENT || 'local'),
        'NODE_ENV': JSON.stringify(env.ENVIRONMENT || 'local')
      }
    }),
  ],
};

