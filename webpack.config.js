const env = process.env;
const file = env.ENVIRONMENT === 'local' ? 'local' : 'production';

require('babel-register');

module.exports = require(`./.webpack/config/${file}`);

